Aliknap helps in deciding how many Alitalia Millemiglia frequent flyer
miles should be converted in e-coupons in order to maximize their euro
value.

It is just an instance of the
[*Unbounded Knapsack Problem*](http://en.wikipedia.org/wiki/Knapsack_problem#Unbounded_knapsack_problem):
the implementation is taken from
[Rosetta Code](http://rosettacode.org) and adapted to use the Alitalia
(2012) miles/e-coupon mapping.

Basic usage:

    $ python aliknap.py 107000
    EUR: 580 (miles 107000). 2 e-coupons: [('26000s', 1), ('81000s', 1)]

Help is available as `python aliknap.py -h`


